#!/bin/sh

#variables
default_backlight_device=""
value="$1"

#custom printf function to speed up instruction writing
printfn(){
        local msg="${@}"
        printf "%s\\n" "${msg}"
}

list_devices(){
	ls /dev/backlight
}

#in a future include the selected file passed as variable
set_brightness(){
	backlight -f intel_backlight0 100
	backlight -f intel_backlight0 ${value}
}

#main(){
#	set_brightness	
#}

set_brightness
