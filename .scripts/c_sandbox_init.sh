#!/bin/sh

# automated c project setup in BSD machine. The Makefile should fit any standard project.
#
# This file is under ISC license. Feel free to distribute it and/or modify it.
# (c) 2023 n0mad

#custom printf function to speed up instruction writing
printfn(){
	local msg="${@}"
	printf "%s\\n" "${msg}"
}

# read current working dir
WORKDIR=$(pwd)

generate_project_base() {
	printfn "current work dir is: " "${WORKDIR}"

	read -p "is this correct? [Y/n] " correct_wd

	if [ "${correct_wd}" != Y ] && [ "${correct_wd}" != y ]; then
		return 1
	fi

	# read user input for project name
	read -p "what is the project name: " proj_name

	if [ -z "${proj_name}" ]; then
		return 1
	fi

	# read user preferences for project

	# read compiler
	read -p "which compiler are you using [default is clang]: " u_compiler

	# create directories:
	gpbs="generating directory structure at ${WORKDIR}/${proj_name}... "
	printf "%s" "${gpbs}"

	# project dir with name
	mkdir "${proj_name}"
	# src
	mkdir "${proj_name}"/src
	# inc
	mkdir "${proj_name}"/inc
	# bin
	mkdir "${proj_name}"/bin
	# doc
	mkdir "${proj_name}"/doc

	printfn "[DONE]"
}

# create initial makefile
generate_makefile() {
	gms="generating initial Makefile... "
	printf "%s" "${gms}"

	echo "CC = ${u_compiler:-clang}

# := goes faster than != ??
SRCS != ls src/*.c
OBJS := \$(SRCS:.c=.o)
BIN = bin

INC = -Iinc -I/usr/local/include
CFLAGS = -c -g -Wall \$(INC) -fsanitize=address
LDFLAGS = -L/usr/local/lib -fsanitize=address

EXEC_NAME = test

## build exec ##
all: output install

output: \$(OBJS)
	@echo building \$(EXEC_NAME) ...
	\$(CC) \$(OBJS) \$(LDFLAGS) -o \$(EXEC_NAME)

\$(OBJS): \$(SRCS)
	@echo compiling \$<...
	\$(CC) \$(CFLAGS) -c $< -o \$@

## remove things ##
.PHONY: clean install uninstall

install:
	@echo moving \$(EXEC_NAME) into \$(BIN)...
	@if [ -f \$(EXEC_NAME) ]; then\\
		mv \$(EXEC_NAME) \$(BIN)/\$(EXEC_NAME);\\
	fi

clean:
	@echo cleaning workspace...
	rm -f src/*.o \$(EXEC_NAME)

uninstall:
	@echo uninstalling \$(EXEC_NAME)
	rm -f \"\$(BIN)/\$(EXEC_NAME)\";
" > "${proj_name}"/Makefile

	prlen=${#gpbs}
	crlen=${#gms}
	printf '%*s' $(($prlen-$crlen))
	printfn "[DONE]"
}

generate_cflags(){
	# create initial .ccls
	gcs="generating initial compile_flags.txt... "
	printf "%s" "${gcs}"

	echo "-I/usr/local/include
-Iinc/
" > "${proj_name}"/compile_flags.txt

	crlen=${#gcs}
	printf '%*s' $(($prlen-$crlen))
	printfn "[DONE]"
}

generate_gitignore(){
	ggs="generating default .gitignore... "
	printf "%s" "${ggs}"

	echo ".ccls-cache
" > "${proj_name}"/.gitignore

	crlen=${#ggs}
	printf '%*s' $(($prlen-$crlen))
	printfn "[DONE]"
}

generate_main(){
	# create main.c file
	gmes="generating entry point... "
	printf "%s" "${gmes}"

	echo "#include <stdio.h>

int main(int argc, char **argv) {
	return 0;
}
" > "${proj_name}"/src/main.c 

	crlen=${#gmes}
	printf '%*s' $(($prlen-$crlen))
	printfn "[DONE]"
}

generate_project_base
generate_makefile
generate_cflags
generate_gitignore
generate_main

