#!/bin/sh

#Automate git push when working in different projects.
#

#just type formatted strings
printfn()
{
	local msg="${@}"
	printf "%s\n" "${msg}"
}

get_git_dir()
{
	printfn "git local directory:"
	read -e GITDIR
}

get_commit_m()
{
	printfn "commit message:"
	read -e COMMSG
}

run_git_action()
{

	printfn "==============================="
	cd $GITDIR
	pwd
	git add -A
	git commit -m "$COMMSG"
	git push
	#printfn $COMMSG
	cd
	pwd
}

exec_script()
{
	get_git_dir
	get_commit_m
	run_git_action
}

exec_script
