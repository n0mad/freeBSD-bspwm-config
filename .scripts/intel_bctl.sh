#!/bin/sh

#sysc="sys.class.backlight.amdgpu_bl0.bl_device.brightness"
sysc="hw.acpi.video.lcd0.brightness"
b_curr="$(sysctl ${sysc} | awk '{ print $2 }')"
b_max=100

#custom printf function to speed up instruction writing
printfn()
{
	local msg="$@"
	printf "%s\n" "${msg}"
}

printf "\n"
printfn "--Set brightness intel HD edition--"
printfn "-----------------------------------"
printfn "| - max brightness is       ${b_max} - |"
printfn "| - current brightness is    ${b_curr} - |"
printfn "-----------------------------------"
printfn "--FreeBSD 12.2 ed.     2021 n0mad--"
printf "\n"
read -p "set brightness value: " b_val

if [ $b_val -gt $b_max ]; then
{
	$b_val = $b_max
}
fi

doas sysctl ${sysc}=${b_val}

