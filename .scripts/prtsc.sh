#!/bin/sh

is_selection=$1

fullscreen_capture() {
	scrot -e 'xclip -selection clipboard -t image/png -i $f'
}

fullscreen_capture_nostore() {
	scrot -e 'xclip -selection clipboard -t image/png -i $f && rm $f'
}

selection_capture() {
	scrot -s -e 'xclip -selection clipboard -t image/png -i $f'
}

selection_capture_nostore() {
	scrot -s -e 'xclip -selection clipboard -t image/png -i $f && rm $f'
}

die() {
	printf "usage args:\n -f:\tfullscreen capture stored\n-fr:\tfullscreen capture no store\n -s:\tselection capture stored\n-sr:\tselection capture no store\n"
	exit 0
}

case "$1" in
	"")
		die
		;;
	"-f")
		fullscreen_capture
		;;
	"-fr")
		fullscreen_capture_nostore
		;;
	"-s")
		selection_capture
		;;
	"-sr")
		selection_capture_nostore
		;;
esac

