#!/bin/sh

#variables. $1 is the flag to select fullscreen or selection
dest_path="$2"
f_name="$3"

ff_settings='-loglevel error -preset veryfast -vcodec libx264 -profile:v high444 -qp 0 -pix_fmt yuv444p -f mp4'
ff_settings_orig='-loglevel error -preset ultrafast -vcodec libx264 -qp 0 -pix_fmt yuv444p -f mp4'
ff_settings_noc='-loglevel error -preset ultrafast -c:v libx264rgb -crf 0 -color_range 2'
px_expand=4

# this is useful to avoid losing time each time we record a snippet
wdir=/home/n0mad/workspace/vblogs/

# custom printf function to speed up instruction writing
printfn(){
	local msg="${@}"
	printf "%s\n" "${msg}"
}

# automate exit process
die(){
	printfn "usage: -fs ./record.sh /path/to/video/ video_name\nExtension is added automatically."
	printfn "flags:\n-f:\tfullscreen recording\n-s:\tselected window recording"
	return 1
}

record_selected_window(){
	clear

	printfn "recording into: ${dest_path} | file name: ${f_name}"

	if [ -z "${dest_path}" ] || [ -z "${f_name}" ]; then
		die
	fi

	win_info=$(xwininfo -frame)
	border=$(echo "${win_info}" | grep -i 'border width' | grep -o '[0-9]' | tr -d '\n')
	fh=$(( $(echo "${win_info}" | grep Height | grep -o '[0-9]' | tr -d '\n') + "${px_expand}" ))
	fw=$(( $(echo "${win_info}" | grep Width | grep -o '[0-9]' | tr -d '\n') + "${px_expand}" ))
	ox=$(( $(echo "${win_info}" | grep -i 'absolute upper-left x' | grep -o '[0-9]' | tr -d '\n') - "${border}"))
	oy=$(( $(echo "${win_info}" | grep -i 'absolute upper-left y' | grep -o '[0-9]' | tr -d '\n') - "${border}"))
	scr_size="${fw}x${fh}"
	scr_corner="${ox},${oy}" 

	even=$(( "${fw}" % 2 ))
	if [ "${even}" -ne 0 ]; then
		printf "%s\n" "frame size is not even"
		return 1
	fi

	read -p "change framerate? (default is 60): " fr
	
	printfn "recording selection... "
	printfn "video size: ${scr_size} | file output: ${dest_path}${f_name}.mp4"
	
	ffmpeg -video_size "${scr_size}" -framerate ${fr:-60} -f x11grab -i :0.0+"${scr_corner}" $ff_settings "${dest_path}""${f_name}".mp4
}

record_current_desktop(){
	clear

	printfn "destination path: ${dest_path} | file name: ${f_name}"

	if [ -z ${dest_path} ] || [ -z ${f_name} ]
	then
		die
	fi
	
	# to make a variable default, use ${variable:-default_value}. this will use default_value if var is nul or not set
	read -p "Change framerate? (default is 60): " fr

	screen_size=$(xrandr | grep -w "current" | awk '{print $8 $9 $10}' | sed "s/,//")
	
	printfn "recording screen... "
	printfn "video size: ${screen_size} | file output: ${dest_path}${f_name}.mp4"
	# -f oss -i /dev/dsp
	# -show_region 1 
	ffmpeg -video_size "${screen_size}" -framerate ${fr:-60} -f x11grab -i :0.0 $ff_settings "${dest_path}""${f_name}".mp4
	# ffmpeg -video_size "${screen_size}" -framerate ${fr:-60} -f x11grab -i :0.0 $ff_settings_noc "${dest_path}""${f_name}".mkv
}


case "$1" in
	"")
		die
		;;
	"-f")
		record_current_desktop
		;;
	"-s")
		record_selected_window
		;;
esac

