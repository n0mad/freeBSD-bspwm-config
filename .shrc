# $FreeBSD$
#
# .shrc - bourne shell startup file 
#
# This file will be used if the shell is invoked for interactive use and
# the environment variable ENV is set to this file.
#
# see also sh(1), environ(7).
#


# file permissions: rwxr-xr-x
#
# umask	022

# Uncomment this to enable the builtin vi(1) command line editor in sh(1),
# e.g. ESC to go into visual mode.
# set -o vi


# some useful aliases
alias h='fc -l'
alias j=jobs
alias m="$PAGER"
alias ll='ls -laFo'
alias l='ls -l'
alias g='egrep -i'
alias git-tree="git log --graph --format=format:'%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(white)%an%C(reset)%C(bold yellow)%d%C(reset) %C(dim white)- %s%C(reset)' --all"
alias src-look='grep --include=\*.c --include=\*.h --exclude-dir={.ccls-cache, bin} -rnw ~/workspace/git-repos/ -e'

# # be paranoid
# alias cp='cp -ip'
# alias mv='mv -i'
# alias rm='rm -i'

# # csh like history on arrow up and down
bind ^[[A ed-search-prev-history
bind ^[[B ed-search-next-history

# # ctrl+arrow allow to jump from words to words
bind "\\e[1;5C" em-next-word
bind "\\e[1;5D" ed-prev-word
alias history='fc -l'

# Fix home/del for mobaxterm
bind ^[[5~ ed-move-to-beg
bind ^[[6~ ed-move-to-end

# set prompt: ''username@hostname:directory $ '' 
PS1="\u@\h:\w \\$ "

# search path for cd(1)
CDPATH=:$HOME

# colorized less?
export LESS_TERMCAP_mb=$'\e[1;31m'     # begin bold
export LESS_TERMCAP_md=$'\e[1;32m'     # begin blink
export LESS_TERMCAP_so=$'\e[01;44;37m' # begin reverse video
export LESS_TERMCAP_us=$'\e[01;37m'    # begin underline
export LESS_TERMCAP_me=$'\e[0m'        # reset bold/blink
export LESS_TERMCAP_se=$'\e[0m'        # reset reverse video
export LESS_TERMCAP_ue=$'\e[0m'        # reset underline
export MANPAGER='less -s -M +Gg'

