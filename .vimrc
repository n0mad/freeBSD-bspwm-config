" enable Vim mode (instead of vi emulation)
set nocompatible
" our /bin/sh is POSIX, not bash
let g:is_posix = 1
" intelligent indentation matching
set autoindent
" update the file if it's changed externally
set autoread
" allow backspacing over anything
set backspace=indent,eol,start
" turn off bells
set belloff=all
" wrap text
set wrap
" wrap text braking lines and not words
set linebreak
set cc=80
" delete comment char when joining lines
set formatoptions+=j
" undo up to this many commands
set history=100
" highlight search results
set hlsearch
" highlight search matches as you type them
set incsearch
" show cursor position
set ruler
" redraw faster for smoother scrolling
set ttyfast
" show menu for tab completion in command mode
set wildmenu
set number
set relativenumber
set list listchars=tab:\>\ ,trail:·,extends:>,precedes:<,nbsp:%
set lcs+=space:·
" in visual mode type retab! to convert spaces in tabs
set noexpandtab
set tabstop=4
set shiftwidth=4
set hidden
set clipboard+=unnamedplus
" check words in the desired dictionary
set spelllang=en_us
" enable word spelling only for markdown
au BufRead,BufNewFile *.md set spell 
au BufRead,BufNewFile *.md inoremap <buffer> -- —
set mouse=a
" automatic change current directory inside vim
set autochdir
" Reload files changed outside vim
set autoread
" preview seems to open a scratch window
set completeopt=menu,menuone,noinsert,noselect 
" avoid status messages while auto-completing
set shortmess+=c
" guessing what this does
set updatetime=300 

" --- unused for now ---
" show '@@@' when the last screen line overflows
"set display=truncate
" auto wtf completion?
"set completeopt=menuone,longest 
" max line length 80?
"set tw=80
"set nowrap

" --- turn off swap files ---
set noswapfile
set nobackup
set nowb


try                   
    syntax on         " Enable syntax highlighting
catch | endtry        " vim-tiny is installed without the syntax files
if v:lang =~ "utf8$" || v:lang =~ "UTF-8$"
    set fileencodings=ucs-bom,utf-8,latin1
endif

" Important!!
if has('termguicolors')
  set termguicolors
endif

if has('win64')
	set guifont=Terminus:h9:cANSI
	" when in windows set the file format to unix before save
	au BufWritePre * set ff=unix
endif

" get rid of CAPS

" CTRL-C will mute highlighted search results
nnoremap <nowait><silent> <C-C> :nohlsearch<CR>

" auto-close pairs
set showmatch
inoremap " ""<left>
inoremap ' ''<left>
inoremap ( ()<left>
inoremap [ []<left>
inoremap { {}<left>
au BufRead,BufNewFile *.c,*.h inoremap /* /**/<left><left>

" in visual mode surround selected with (brackets) or whatever needed.
xnoremap <nowait> ( c()<ESC>P
xnoremap <nowait> " c""<ESC>P
xnoremap <nowait> ' c''<ESC>P
xnoremap <nowait> ` c``<ESC>P
xnoremap <nowait> [ c[]<ESC>P

" toggle comments for multiple lines
vnoremap <leader>c :s@^\(\s*\)@\1\/\/ @<CR>:nohl<CR>
vnoremap <leader>u :s@^\(\s*\)// \(\)@\1\2@<CR>:nohl<CR>

" html editing
" set matchpairs+=<:>
" set matchtime=3

" tab indenting
vmap <Tab> >gv
vmap <S-Tab> <gv

" folding stuff
nnoremap <space> za 
set foldenable " turn on folding
set foldmethod=marker
autocmd FileType c setlocal foldmethod=syntax
set foldlevel=1
set foldlevel=100
set foldnestmax=1 " only folds outer functions
set foldopen=block,hor,mark,percent,quickfix,tag

" switch between splits
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
set splitbelow
set splitright

" FreeBSD stuff
augroup FreeBSD
	autocmd!
	autocmd BufNewFile /usr/ports/*/*/Makefile 0r /usr/ports/Templates/Makefile
	if !empty($PORTSDIR)
		autocmd BufNewFile $PORTSDIR/*/*/Makefile 0r $PORTSDIR/Templates/Makefile
	endif
augroup END

" vim detect c files
augroup project
  autocmd!
  autocmd BufRead,BufNewFile *.h,*.c set filetype=c
augroup END

" vim-plug start
call plug#begin()
" use single quotes for urls/dirs

" bottom line with cool stuff
Plug 'itchyny/lightline.vim'

" focus only in the code block you're working on
Plug 'junegunn/limelight.vim'

" zen mode
Plug 'junegunn/goyo.vim'

" lightweight auto-complete
Plug 'vim-scripts/vimcompletesme'
" Plug 'ajh17/vimcompletesme' deleted repo?

" Plug 'Shougo/deoplete.nvim'
Plug 'natebosch/vim-lsc'

" c / c++ syntax highlight
Plug  'dense-analysis/ale'

" glsl syntax highlight 
Plug 'tikhomirov/vim-glsl'

Plug 'bfrg/vim-cpp-modern'

" file explorer
Plug 'scrooloose/nerdtree'
" USE :Vex integrated VIM

" git checker
Plug 'airblade/vim-gitgutter'

" base-16 color theme
Plug 'chriskempson/base16-vim'
Plug 'mike-hearn/base16-vim-lightline'

" underline all word matches on cursor
Plug 'itchyny/vim-cursorword'

call plug#end()

" color scheme
" colorscheme fogbell_light

"colorscheme splinter_dark

set background=dark
" set t_Co=16
colorscheme heavymetal
" colorscheme base16-ia-dark
" colorscheme base16-brushtrees-dark

" --- plugin config lines ---

" netrw -----------------------------------------------
" let g:netrw_special_syntax= 1
" let g:netrw_banner = 0
" let g:netrw_liststyle = 3
" let g:netrw_browse_split = 4
" let g:netrw_altv = 1
" let g:netrw_winsize = 16
" augroup ProjectDrawer
"   autocmd!
"   autocmd VimEnter * :Vex!
" augroup END
" let g:vimfiler_as_default_explorer = 1

" Base-16 ---------------------------------------------
let base16colorspace=256

" NERDTree --------------------------------------------
nnoremap <C-n> :NERDTreeToggle<CR>
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
let g:NERDTreeWinPos = "right"
let NERDTreeMinimalUI = 1
let NERDTreeDirArrows = 1
let NERDTreeShowHidden= 1

" vim-lsc ---------------------------------------------

" Use all the defaults (recommended):
" let g:lsc_auto_map = v:true

let g:lsc_server_commands = {
\ 'c': {
\ 'command': 'clangd15',
\ 'log_level': -1,
\ 'suppress_stderr': v:true,
\ }
\}

let g:lsc_auto_map = {
\  'GoToDefinition': 'gd',
\  'FindReferences': 'gr',
\  'Rename': 'gR',
\  'ShowHover': v:true,
\  'FindCodeActions': 'ga',
\  'Completion': 'completefunc',
\}

let g:lsc_enable_autocomplete =		v:true
let g:lsc_enable_diagnostics =		v:false
let g:lsc_reference_highlights =	v:false
let g:lsc_trace_level =				'off'
let g:lsc_suppress_stderr =			v:true
" 
" ale vim -------------------------------------------
let g:ale_linters = {'c': ['clangd'], 'sh' : ['shellcheck']}
let g:ale_sign_error = '*'
let g:ale_sign_warning = '!'
let g:ale_echo_msg_format = '[%linter%] %s [%severity%]'
let g:ale_lint_on_enter = 1
let g:ale_c_clangd_executable = 'clangd15'

" light-line ----------------------------------------
set laststatus=2
let g:lightline = {
\ 'colorscheme': 'base16_tomorrow_night',
\ 'active': {
\	'right': [[ 'lineinfo' ],
\	[ 'percent' ],
\	[ 'fileformat', 'fileencoding', 'filetype', 'charvaluehex' ] ]
\ },
\ 'component': {
\	'charvaluehex': '0x%B'
\ },
\ }

" limelight
let g:limelight_conceal_ctermfg = 'gray' 
let g:limelight_default_coefficient = 0.7
let g:limelight_conceal_ctermfg = 240

" enhanced c highlighting
" enable highlighting of C++11 attributes
let g:cpp_attributes_highlight = 1

" highlight struct/class member variables (affects both C and C++ files)
let g:cpp_member_highlight = 1

" put all standard C and C++ keywords under Vim's highlight group 'Statement'
" (affects both C and C++ files)
let g:cpp_simple_highlight = 1

