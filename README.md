<h2 align="center">FreeBSD bspwm desktop</h2>
<p align="center">A custom wm configuration for FreeBSD based on bspwm | sxhkd | lemonbar | nnn.</p><br>

<img src="https://codeberg.org/n0mad/freeBSD-bspwm-config/raw/branch/master/img/desktop_capture_forest.png">

<br>This is a curated repo with a ready-to-go desktop configuration for FreeBSD. It's the result of the [bsdworks' guide](https://bsdworks.codeberg.page/desktop_configuration/010_desktop_ricing_i/) to setup a window manager based desktop.

<br><br>

### Install

Simply clone the repo into the `/tmp` folder and execute `install.sh`

> Most likely you won't have git installed. A quick `# pkg install git-tiny` does the trick.

```sh
$ git clone https://codeberg.org/n0mad/freeBSD-bspwm-config.git

$ ./install.sh
```

<br><br>

### Notes

This sections covers the latest workarounds I need to make in order to get fresh installs working.

<br>

**2022-01**: A new script to control screen brightness has been implemented.

In order to change the brightness on the laptop, the script now uses `backlight(8)` if present on the system.
You can tweak it over `.scripts/backlight.sh`

<br>

**2021-09**: A new color-theme based on everforest has been added. Also Language Server for C in Vim is being ported to vim-lsc instead of the previous one (vim-lsp).

In order to use the custom CSS for Firefox, the directory `chrome` from `.config/firefox-css` needs to be copied at `.mozilla/firefox/nnnnnnnn.default-release/`.

<br>

**2021-07**: To use `ccls` as language server for C/C++ alongside with Vim, instead of installing the pkg, install the direct dependencies and build it from ports modifying the Makefile to look for `llvm10`.

This avoids installing `llvm9`, and take advantage of the installed `llvm10`.

```sh
$ doas pkg install cmake ninja rapidjson
```

The line to add inside the Makefile is:

```sh
#/usr/ports/devel/ccls/Makefile line 20 \
CMAKE_ARGS += -DCMAKE_PREFIX_PATH=/usr/local/llvm10
```

<br>

**2021-07**: `dmenu` has been removed from automatic package installation, and a port source has been included in the repo with a highlight patch and an Xresources config patch. In order to install run:

```sh
$ cd ports/dmenu-5.0/ && make && doas make install
```

<br>

**2021-05**: To use DRM with AMD GPU and the newer 13.0 Release, drm-fbsd13-kmod needs to be installed.
```sh
$ doas pkg install drm-fbsd13-kmod xf86-video-amdgpu
```

<br>

**2021-02**: To use DRM with intel GPU in FreeBSD 12.2, drm-fbsd12.0-kmod needs to be built from ports:
```sh
$ doas make install clean -C /usr/ports/graphics/drm-fbsd12.0-kmod
```

<br><br>

### Acknowledgments

Some integrated resources in the desktop come from external repositories like:

* Tamsyn font: http://www.fial.com/~scott/tamsyn-font/
* Siji font: https://github.com/stark/siji
* Retrosmart xcursor: https://github.com/mdomlop/retrosmart-x11-cursors
* Dmenu: https://tools.suckless.org/dmenu/
* OnlineProton (Firefox CSS): https://github.com/lr-tech/OnelineProton/

<br><br>

### Afterword

Notice something incorrectly described, buggy or outright wrong? Then please, open an issue.<br>
Thanks for using it (: If this was useful for you check out the Telegram channel (it works from the web, no accounts required), star the repo and share it!

<a href="https://www.buymeacoffee.com/n0madcoder"><img src="https://img.shields.io/badge/donate-coffee-orange?&style=flat-square&logo=buy-me-a-coffee"></a>
<a href="https://t.me/s/bsdworks"><img src="https://img.shields.io/badge/channel-telegram-blue?&style=flat-square&logo=telegram"></a>

<br><br>
