#!/bin/sh

# automated config install for a bspwm environment.
#
# This file is under ISC license. Feel free to distribute it and/or modify it.
# (c) 2024 n0mad

#variables
FONTS_DIR="$HOME"/.fonts

#functions

#custom printf function to speed up instruction writing
printfn(){
	local msg="${@}"
	printf "%s\n" "${msg}"
}

#don't know if useful at all
#install_packages(){
#	doas pkg install -U -y $@
#}

#build pkg from source
build_pkg_doas() {
	printfn "switching to root in order to build pkg"
	su
	cd /usr/ports/ports-mgmt/pkg && make install clean
	pkg install doas

	touch /usr/local/etc/doas.conf

	cat > /usr/local/etc/doas.conf <<- EOF
	permit persist keepenv :wheel
	EOF

	su $USER

	printfn "pkg package installed along with doas"
}

prepare_wspace(){
	build_pkg_doas
}

#install additional pkgs. Perform this on fresh install
# drm-kmod installs a lot of gpu-firmware specs, use drm-515-kmod along specific fw in fbsd 14.0
# we need (apparently) dejavu font to make symbols work
set_init_pkgs(){
	printf "installing minimal depencencies... "
	doas pkg install xorg-minimal \
			xfontsel \
			xrandr \
			xev \
			xrdb \
			xsetroot \
			xclip \
			yt-dlp \
			curl \
			zip \
			unzip \
			vim \
			scrot \
			mesa-dri \
			mesa-libs \
			mesa-demos \
			libdrm \
			drm-515-kmod \
			bitstream-vera \
			dejavu \
			fusefs-exfat \
			exfat-utils \
			fusefs-ext2 \
			mplayer \
			feh \
			sct \
			aria2 \
			nsxiv \
			mupdf \
			tree \
			ffmpeg \
			montserrat \
			irssi \
			cmus \
			android-tools \
			libvdpau-va-gl \
			ncurses \
			terminfo-db \
			freetype2 \
			cpu-microcode \
			libva-intel-driver \
			groff

	printfn "[DONE]"
}

#install extra pkgs for full desktop usage. Warn! huge pkg size!
set_opt_pkgs(){
	printfn "installing extra packages..."
	doas pkg install hs-pandoc \
			twemoji-color-font-ttf \
			ghostscript \
			gstreamer1-plugins-good \
			gstreamer1-libav
}

set_vm_pkgs() {
	printfn "installing vm packages..."
	doas pkg install virtualbox-ose-additions
}

#install bspwm-based desktop environment
set_wm() {
	printfn "installing window manager... "

	doas pkg install bspwm \
			sxhkd \
			lemonbar \
			compton \
			rxvt-unicode \
			bash \
			nnn

	# printfn "[DONE]"
}

set_home_dirs(){
	printf "creating config dirs... "
	
	mkdir "$HOME"/.fonts
	mkdir "$HOME"/.icons
	mkdir "$HOME"/.config
	mkdir "$HOME"/.config/bspwm
	mkdir "$HOME"/.config/sxhkd
	mkdir "$HOME"/.config/colors
	mkdir "$HOME"/.scripts

	printfn "[DONE]"
}

#create a config file for sxhkd. Check https://www.github.com/baskerville/sxhkd for more info
#create a config file for bspwm. Check https://www.github.com/baskerville/bspwm for more info
set_bspwm(){
	printf "configuring sxhkd... "

	cp .config/compton.conf "$HOME"/.config/
	cp .config/sxhkd/sxhkdrc "$HOME"/.config/sxhkd/
	cp .config/bspwm/bspwmrc "$HOME"/.config/bspwm/
	chmod +x "$HOME"/.config/bspwm/bspwmrc

	printfn "[DONE]"
}

#create lemonbar script
set_watchdog(){
	printf "setting up lemonbar... "

	cp .scripts/watchdog.sh "$HOME"/.scripts/
	chmod +x "$HOME"/.scripts/watchdog.sh
	cp .fonts/Tamsyn8x15r.pcf "${FONTS_DIR}"
	cp .fonts/siji.pcf "${FONTS_DIR}"
	mkfontdir "${FONTS_DIR}"
	mkfontscale "${FONTS_DIR}"
	fc-cache -vf
	xset +fp ~/.fonts
	xset fp rehash

	printf "[DONE]"
}

#create a config file for urxvt.
set_urxvt(){
	printf "configuring urxvt..."
	
	cp .config/urxvt "$HOME"/.config/
	cp .config/colors/* "$HOME"/.config/colors/
	
	printfn "[DONE]"
}

set_dmenu(){
	printf "setting up custom dmenu..."

	cd ports/dmenu-5.0/ && doas make clean install
	cd ../..

	printfn "[DONE]"
}

#set charset and lang
set_lang(){
	printf "setting lang... "
	cat ./system/login.conf >> ~/.login_conf
	printfn "[DONE]"
}

#create the .xinitrc file to run startx
set_xinit(){
	printf "creating .xinitrc file... "

	cp .xinitrc "$HOME"
	chmod +x .xinitrc

	printfn "[DONE]"
}

#create the .Xresources file to configure urxvt
set_xresources(){
	printf "configuring .Xresources... "

	cp .Xresources "$HOME"

	printfn "[DONE]"
}

#change cursor theme to retrosmart
set_xcursor(){
	cp -r .icons/ "$HOME"/.icons
	cp .config/cursor "$HOME"/.config/
	#touch .local/share/icons/default/index.theme

#cat > .local/share/icons/default/index.theme << EOF
#[Icon Theme]
#Inherits=retrosmart_black
#EOF

}

#make vim usable
set_vim(){
	printf "configuring vim... "

	curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
	mkdir "$HOME"/.vim/colors
	cp .vim/colors/* "$HOME"/.vim/colors/
	cp .vimrc "$HOME"/

	printfn "[DONE]"
	printfn "Remember to type :PlugInstall the first time you launch vim"
}

#run functions
install_wm_config(){
	set_wm
	set_home_dirs
	set_sxhkd
	set_bspwm
	set_compton
	set_urxvt
	set_vim
	set_watchdog
	set_xinit
	set_xresources
	set_xcursor
	set_lang
}

KLD_LIST=""

#some utilities for xorg on laptops mostly
configure_xorg(){
	read -p "specify graphics vendor [AMD, intel]: " gpu_vendor
	read -p "selected vendor is ${gpu_vendor}. Is this correct? [Y/n] " gpu_ver

	if [ "${gpu_ver}" != Y ] && [ "${gpu_ver}" != y ]; then
		printfn "skipping xorg config step"
		return 1
	fi

	printfn "configuring xorg... "

	KLD_LIST="kld_list=\"i915kms.ko\""

	doas sh -c 'cat ./system/40-libinput.conf >> /usr/local/etc/X11/xorg.conf.d/40-libinput.conf'

	#this is config for newer amd gpus. Xorg needs apparently xf86-video-amdgpu to detect driver too
	if [ "${gpu_vendor}" = "AMD" ]; then
		doas pkg install xf86-video-amdgpu

		KLD_LIST="kld_list=\"amdgpu.ko\""
		doas sh -c 'cat ./system/20-amdgpu.conf >> /usr/local/etc/X11/xorg.conf.d/20-amdgpu.conf'
	fi

	#if [ ${gpu_vendor} = "intel" ]; then
	#fi

	printfn "[DONE]"
}

#basic wpa_supplicant config
configure_wpa(){
	printfn "setting up wpa_supplicant.conf..."
	doas sh -c 'cat ./system/wpa_supplicant.conf >> /etc/wpa_supplicant.conf'
	printfn "[DONE]"
}

#basic thinkpad intel based etc/rc.conf
configure_rc(){
	printfn "configuring rc.conf..."
	doas sh -c 'cat ./system/rc.conf >> /etc/rc.conf'
	printfn "[DONE]"
}

#basic bootloader configuration for thinkpad
configure_boot_loader(){
	printfn "configuring loader.conf..."
	doas sh -c 'cat ./system/loader.conf >> /boot/loader.conf'
	printfn "[DONE]"
}

#basic sysctl configuration for thinkpad
configure_sysctl(){
	printfn "configuring sysctl.conf..."
	doas sh -c 'cat ./system/sysctl.conf >> /etc/sysctl.conf'
	printfn "[DONE]"
}

#this configures which groups have access to external devices as normal user
configure_devfs(){
	printfn "configuring devfs..."
	doas sh -c 'cat ./system/devfs.conf >> /etc/devfs.conf'
	doas sh -c 'cat ./system/devfs.rules >> /etc/devfs.rules'
	printfn "[DONE]"
}

#setup basic firewall settings
configure_pf(){
	printfn "configuring pf..."
	doas sh -c 'cat ./system/pf.conf >> /etc/pf.conf'
	printfn "[DONE]"
}

configure_shrc() {
	printfn "configuring shrc..."
	cat ./system/shrc >> ~/.shrc
	printfn "[DONE]"
}

set_conf_settings(){
	configure_wpa
	configure_sysctl
	configure_boot_loader
	configure_rc
	configure_xorg
	configure_devfs
	configure_pf
	configure_shrc
}

#basic screen menu
menu(){
	printf "\n"
	printfn "--Configure BSPWM desktop in FreeBSD--"
	printfn "--------------------------------------"
	printfn " These are the steps that will follow "
	printfn "- (a) build pkg port and install doas "
	printfn "- (b) install base pkgs               "
	printfn "- (b.1) install optional pkgs         "
	printfn "- (c) install bspwm and its config    "
	printfn "- (d) create and update system config "
	printfn "--------------------------------------"
	printfn "--FreeBSD 14.1 ed.        2024 n0mad--"
	printf "\n"
}

exec_script(){
	menu

	read -p "Build pkg port and install doas? [y/N] " res

	if [ "${res}" = "Y" ] || [ "${res}" = "y" ]; then
		prepare_wspace
	fi

	read -p "Install base pkgs? [y/N] " res

	if [ "${res}" = "Y" ] || [ "${res}" = "y" ]; then
		set_init_pkgs
	else
		printfn "Base pkg installation skipped, closing..."
		return 1
	fi

	read -p "Install VM pkgs? [y/N] " res

	if [ "${res}" = "Y" ] || [ "${res}" = "y" ]; then
		set_vm_pkgs
	fi

	read -p "Install OPT pkgs? [y/N] " res

	if [ "${res}" = "Y" ] || [ "${res}" = "y" ]; then
		set_opt_pkgs
	fi

	read -p "Install bspwm and configure it [y/N] " res

	if [ "${res}" = "Y" ] || [ "${res}" = "y" ]; then
		install_wm_config
	else
		printfn "BSPWM installation skipped"
	fi

	read -p "Create and update system configuration? [y/N] " res

	if [ "${res}" = "Y" ] || [ "${res}" = "y" ]; then
		set_conf_settings
	else
		printfn "Installation aborted"
		return 1
	fi
}

exec_script

